with Ada.Text_IO,Ada.Integer_Text_IO; use Ada.Text_IO,Ada.Integer_Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with d_trie;
with dqueue;






package d_carta is
	type carta is limited private;
	type tcategoria is (Entrant, Primer, Segon, Postres, Begudes);
	type tcomentaris is (Bo, Mitja, Dolent);

	subtype component is Character range '@'..'Z';
	type index IS RANGE 1..5;
	type tcodi IS ARRAY (index) OF component;



	procedure carta_buida (c: out carta);
	procedure posar_element (c: in out carta;
			cat: in tcategoria; k: in tcodi;
			nom: in Unbounded_String);
	procedure imprimir_elements(c: in carta; cat: in tcategoria);
	procedure eliminar_element(c: in out carta; cat: in tcategoria;
			k: in tcodi);
	procedure posar_comentari (c: in out carta;
			cat: in tcategoria; k: in tcodi;
			q: in tcomentaris;
			comentari: in Unbounded_String);
	function consultar_comentari (c: in carta;
			cat: in tcategoria; k: in tcodi;
			q: in tcomentaris)
			return Unbounded_String;
	function existeix_comentari (c: in carta;
			cat: in tcategoria; k: in tcodi;
			q: in tcomentaris)
			return Boolean;
	procedure eliminar_comentari (c: in out carta;
			cat: in tcategoria; k: in tcodi;
			q: in tcomentaris);

private
	type comentari is record --Comentari en si
		c : Unbounded_String;
	end record;
	package myqueue is new dqueue (item => comentari); -- Declaració coa
	use myqueue;
	type pqueue is access queue; -- Punter a una coa
	type arrayCoes is array (tcomentaris) of pqueue;

	type tplat is --El plat té un nom i tres punters de coa (un per cada tipus de comentari).
	record
		nom : Unbounded_String;
		comentaris : arrayCoes;
	end record;




	package mytrie is new d_trie (item => tplat, key_component => component, key_index => index, key => tcodi);
	use mytrie;

	type ptrie is access set; -- Punter a un trie
	type categories is array (tcategoria) of ptrie;




	type carta is
	record
		ctgs :  categories;--5 punters a tries.
	end record;






end d_carta;
