with Ada.Text_IO,Ada.Integer_Text_IO,Ada.Command_Line; use Ada.Text_IO,Ada.Integer_Text_IO,Ada.Command_Line;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with d_carta; use d_carta;

procedure main is

    c : carta;
    cat : tcategoria;
    k : tcodi;
    nomPlat : Unbounded_String;
    q : tcomentaris;
    comment : Unbounded_String;
begin
    carta_buida(c);
    k := ('A','B','C','D','@');
    cat := Primer;
    q := Bo;
    nomPlat := To_Unbounded_String("Bistec");
    posar_element(c,cat,k,nomPlat);

    nomPlat := To_Unbounded_String("Arros Brut");
    k := ('A','J','K','D','@');
    posar_element(c,cat,k,nomPlat);


    for i in tcategoria loop
        imprimir_elements(c,i);
    end loop;




end main;
