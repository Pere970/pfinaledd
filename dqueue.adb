-- dqueue.adb
package body dqueue is

	--buida la coa
	procedure empty (qu: out queue) is
		p: pcell renames qu.p;
		q: pcell renames qu.q;
	begin -- empty
		p:= null;
		q:= null;
	end empty;

	--funció que diu si és buida o no
	function is_empty (qu: in queue) return boolean is
		q: pcell renames qu.q;
	begin
		return q/= null;
	end is_empty;

	--retorna el primer element de la coa
	function get_first (qu: in queue) return item is
		q: pcell renames qu.q;
	begin -- get_first (qu: in queue)
		return q.x;
	exception
		when constraint_error => raise Bad_Use; --si la pila fos buida
	end get_first;

	--ficar un item a la coa
	procedure put (qu: in out queue; x: in item) is
		p: pcell renames qu.p;
		q: pcell renames qu.q;
		r: pcell;
	begin -- put
		r:= new cell;
		r.all := (x, null);
		if p = null then --si la coa és buida
			p:= r;
			q:=r;
		else
			p.next := r; --afegim nova celda al final de la coa
			p:= r; --ara q apunta a r(la nova celda afegida)
		end if;
	exception
		when storage_error => raise Space_Overflow;
	end put;

	--elimina el primer item de la coa
	procedure rem_first(qu: in out queue) is
		p: pcell renames qu.p;
		q: pcell renames qu.q;
	begin -- rem_first
		q:= q.next;
		if q = null  then
			p:= null;
		end if;
	exception
		when constraint_error => raise Bad_Use;
	end rem_first;

	--retorna true si la coa només te un element
	function is_last_item (qu: in queue )return boolean is
		p: pcell renames qu.p;
		q: pcell renames qu.q;
	begin -- is_last_item
		return q /= null and p /= null and q = p;
	end is_last_item;
end dqueue;
