with Ada.Text_IO;
use Ada.Text_IO;


package body d_carta is

procedure carta_buida (c: out carta) is

begin
    for i in tcategoria loop
        c.ctgs(i) := new set;
        mytrie.empty(c.ctgs(i).all);
    end loop;
end carta_buida;

procedure posar_element (c: in out carta; cat: in tcategoria; k: in tcodi; nom: in Unbounded_String) is
    plat : tplat;
begin
    plat.nom := nom;
    for i in tcomentaris loop
        plat.comentaris(i) := new queue;
        myqueue.empty(plat.comentaris(i).all);
    end loop;

    mytrie.put(c.ctgs(cat).all, k, plat);

end posar_element;

procedure posar_comentari (c: in out carta; cat: in tcategoria; k: in tcodi; q: in tcomentaris; comentari: in Unbounded_String) is
    plat : tplat;
    comment : d_carta.comentari;
begin

    plat := mytrie.get(c.ctgs(cat).all,k);
    comment.c := comentari;
    myqueue.put(plat.comentaris(q).all,comment);


end posar_comentari;

function consultar_comentari (c: in carta; cat: in tcategoria; k: in tcodi; q: in tcomentaris) return Unbounded_String is
    plat : tplat;
    comment : d_carta.comentari;
begin
    plat := mytrie.get(c.ctgs(cat).all,k);
    if existeix_comentari(c,cat,k,q) then
        comment := myqueue.get_first(plat.comentaris(q).all);
    end if;
    return comment.c;

end consultar_comentari;

function existeix_comentari (c: in carta; cat: in tcategoria; k: in tcodi; q: in tcomentaris) return Boolean is
    plat : tplat;
begin
    plat := mytrie.get(c.ctgs(cat).all,k);
    return myqueue.is_empty(plat.comentaris(q).all);
end existeix_comentari;

procedure eliminar_comentari (c: in out carta; cat: in tcategoria; k: in tcodi; q: in tcomentaris) is
    plat : tplat;
begin
    plat := mytrie.get(c.ctgs(cat).all,k);
    myqueue.rem_first(plat.comentaris(q).all);
end eliminar_comentari;

procedure eliminar_element(c: in out carta; cat: in tcategoria; k: in tcodi) is

begin
    mytrie.remove(c.ctgs(cat).all,k);
end eliminar_element;

--procedure imprimir_comentaris(q: in queue) is
--begin
--end imprimir_comentaris;


procedure imprimir_elements(c: in  carta; cat: in tcategoria) is
    it : iterator;
    plat : tplat;
    iteracio : integer := 1;
    aux_k : tcodi;
begin
    case cat is
        when Entrant =>
            Put_Line("******ENTRANTS******");
        when Primer =>
            Put_Line("*******PRIMER*******");
        when Segon =>
            Put_Line("*******SEGON********");
        when Postres =>
            Put_Line("******POSTRES*******");
        when Begudes =>
            Put_Line("******BEGUDES*******");
    end case;
    new_line;
    
    first_it(c.ctgs(cat).all,it);
    while (is_valid_it(it)) loop
        get_it(c.ctgs(cat).all,it,aux_k);
        plat := mytrie.get(c.ctgs(cat).all,aux_k);
        Put_Line("PLAT "&iteracio'img&" : "&To_string(plat.nom));
        put("CODI PLAT: ");
        for i in aux_k'range loop
            put(aux_k(i)'img);
        end loop;
        new_line;
        new_line;
        --Put_Line("COMENTARIS: ");
        --for i in tcomentaris loop
        --    if existeix_comentari(c,cat,aux_k,i) then
        --
        --    end if;
        --end loop;
        next_it(c.ctgs(cat).all,it);
        iteracio := iteracio + 1;

    end loop;
    Put_Line("************************");
end imprimir_elements;


end d_carta;
