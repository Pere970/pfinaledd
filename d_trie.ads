generic
	type item is private;
	type key_component is (<>);
	type key_index IS RANGE <>;
	type key IS ARRAY (key_index) OF key_component;
package d_trie is
	type set is limited private;
	type tipo_node is (int,final);
	Space_Overflow: Exception;
	no_exists : exception;
	bad_use : exception;
	type path is private;
	type iterator is private;



	--Mètodes Trie
	procedure Empty(s: out set);
	procedure Put (s:in out set; k: in key; x: in item);
	function get (s: in set; k: in key) return item;
	procedure Remove (s: in out set; K: in key);


	--Mètodes Iterador
	procedure first_it(s: in set; it: out iterator);
	procedure next_it(s: in set; it: in out iterator);
	function is_valid_it(it: in iterator) return boolean;
	procedure get_it(s: in set; it: in iterator; k: out key);

private



	type node ;
	type pnode is access node;
	type pitem is access item;

	type path is array (key_index) of pnode;
	type iterator is record
		pth : path;
		k : key;
		i : key_index;
	end record;
	type keyArray is ARRAY (key_component) of pnode;
	type node(nt: tipo_node) is record
		case nt is
			when int =>
				nodes: keyArray;
			when final =>
				p_item : pitem;
		end case;
	end record;
	type set is record
		root: pnode;
	end record;
end d_trie;
