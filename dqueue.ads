--dqueue.ads
generic 
	type item is private;
package dqueue is 
		
		type queue is limited private;

		Bad_Use: exception;
		Space_overflow: exception;

		procedure empty 	(qu: 	out queue);
		procedure put 		(qu: in out queue; x: in item);
		procedure rem_first (qu: in out queue);
		function get_first 	(qu: in queue) return item;
		function is_empty 	(qu: in queue) return boolean;
		function is_last_item (qu: in queue) return boolean;

private 
	type cell;
	type pcell is access cell; --punter a celda

	type cell is 
		record 
			x: 	item;
			next: pcell;
		end record;

	type queue is record 
		p, q: pcell;
	end record;
end dqueue;

