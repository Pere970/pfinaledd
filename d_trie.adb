with Ada.Text_IO; use Ada.Text_IO;

package body d_trie is

mk: constant key_component:= key_component'first;
lastc: constant key_component:= key_component'last;
i0: constant key_index:= key_index'first;

function unico_desc(p: in pnode) return boolean is
	c: key_component;
	nd: integer; -- número de descendientes
begin
	c:= mk; nd:= 0;
	while c/=lastc and nd<2 loop
		if p.nodes(c)/= null then
			nd:= nd+1;
		end if ;
		c:= key_component'succ(c);
	end loop;
-- comprobar el último caso c=lastc
	if p.nodes(c)/= null then
		nd:= nd+1;
	end if ;
	return nd<2;
end unico_desc;


procedure Empty (s: out set) is
	root: pnode renames s.root;
begin -- Empty
	root:= new node(int);
	root.nodes:=(others => NULL);

end Empty;

function get (s: in set; k: in key) return item  is
	root: pnode renames s.root;
	p: pnode;
	i: key_index;
	c: key_component; --copy of k(i)
begin -- Is_in
	p:= root; i:= i0; c:= k(i);
	while c/=mk and p.nodes(c)/=null loop
		p:=p.nodes(c); i:=i+1; c:=K(i);
	end loop;
	if c=mk and p.nodes(mk)/= null then
		return p.nodes(mk).p_item.all;
	else
		raise no_exists;
	end if;

end get;

procedure Put (s: in out set; k: in key; x : in item) is
	root: pnode renames s.root;
	p,r: pnode;
	i: key_index;
	c: key_component;

begin -- Put

	p:= root; i:=i0; c:= k(i);
	while c/= mk loop
		if p.nodes(c) = null then --no existe hijo para c: crearlo
			r:= new node(int);
			r.nodes:=(others => NULL);
			p.nodes(c):= r;
		end if;
		p:=p.nodes(c);
		 i:=i+1;
		  c:=k(i);
	end loop;
	r:= new node(final);
	r.p_item := new item;
	r.p_item.all := x;
	p.nodes(mk):=r; --si p.all(mk)=p anteriormente, la clave ya existía
exception
	when storage_error => raise Space_Overflow;
end Put;

procedure Remove(s: in out set; k: in key) is
	root: pnode renames s.root;
	p, r: pnode; -- para guardar la bifurcación r: puntero
	i: key_index;
	c, cr: key_component; --para guardar la bifurcación cr. caracter
begin
	p:= root; i:= i0; c:= k(i); r:= p; cr:= c;
	while c/=mk and p.nodes(c)/= null loop
		if not unico_desc(p) then
			r:= p;
			cr:= c;
		end if;
	p:= p.nodes(c); i:= i+1; c:= k(i);
	end loop;
	if c=mk and p.nodes(mk)/= null then -- elemento existe
		if unico_desc(p) then -- no es un prefijo de una clave más larga
			r.nodes(cr):= null; -- desenlazar la rama
		else
			p.nodes(mk):= null; -- quitar sólo la rama $, porque es prefijo
		end if ;
	end if ;
end Remove;



--MÈTODES DEL Trie

--busca el primer nodo no nulo
procedure firstbranch_it (p: in pnode; c: out key_component; found: out boolean) is
begin
	 -- mk era el key_component’first. Y marca el final de las claves
	 c:= mk; found:= (p.nodes(c)/=null);
	 while c < lastc and not found loop
		 c:= key_component'succ(c);
		 found:= (p.nodes(c)/=null);
	 end loop;
end firstbranch_it;

--busca siguiente nodo no vacío desde el último carácter del encontrado
procedure nextbranch_it(p: in pnode; c: in out key_component; found:out boolean) is
begin
	found:= false;
	while c<lastc and not found loop
		c:= key_component'succ(c);
		found:= (p.nodes(c)/=null);
	end loop;
end nextbranch_it;


procedure first_it(s: in set; it: out iterator) is
	root: pnode renames s.root;
	pth: path renames it.pth;
	k: key renames it.k;
	i: key_index renames it.i;
	c: key_component;
	p: pnode;
	found: boolean;
begin
	p:= root; i:= i0;
	firstbranch_it(p, c, found); --busca primer nodo no nulo
	while found and c/=mk loop
		pth(i):= p; k(i):= c; i:= i+1;
		p:= p.nodes(c);
		firstbranch_it(p, c, found);
	end loop;
	pth(i):= p; k(i):= mk; --autopuntero y final a la clave
end first_it;


procedure next_it(s: in set; it: in out iterator) is
	root: pnode renames s.root;
	pth: path renames it.pth;
	k: key renames it.k;
	i: key_index renames it.i;
	c: key_component;
	p: pnode;
	found: boolean;
begin
	if k(i0)=mk then raise bad_use; end if ;
		p:= pth(i); c:= k(i); --posición y carácter
		nextbranch_it(p, c, found);
	while not found and i>1 loop --busco siguiente bifurcación hacia arriba
		 i:= i-1; p:= pth(i); c:= k(i);
		 nextbranch_it(p, c, found);
	end loop;
	while found and c/=mk loop --vuelvo a bajar para abajo.
		 pth(i):= p; k(i):= c; i:= i+1;
		 p:= p.nodes(c);
		 firstbranch_it(p, c, found);
	end loop;
	pth(i):= p; k(i):= mk;
end next_it;

--busca el primer nodo no nulo




function is_valid_it(it: in iterator) return boolean is
	k: key renames it.k;
begin
	return k(i0)/=mk;
end is_valid_it;

procedure get_it(s: in set; it: in iterator; k: out key) is
begin
	if it.k(i0)=mk then
		raise bad_use;
 	end if ;
	k:= it.k;
end get_it;



end d_trie;
